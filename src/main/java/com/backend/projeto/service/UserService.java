package com.backend.projeto.service;

import com.backend.projeto.entity.User;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;

public interface UserService {

    Page<User> findAllPageble(int page, int size);

    User create(User user);

    User update(User user);

    void delete(long id);

    Optional<User> findById(long id);
}
